import React from "react";
import { Link } from "react-router-dom";

function Breadcrumbs(props) {

    let link = props.link;

		return (
      <div className="breadcrumbs">
        <div className="cntr">
          <ul className="breadcrumbs-item">
            <li><Link to="/">HOME</Link></li>
            <li><span>{link}</span></li>
          </ul>
        </div>
      </div>
		)

}

export default Breadcrumbs;
