import React, { Component } from 'react';


class Login extends Component {
  constructor(props) {
    super(props);
    this.handleShowForm = this.handleShowForm.bind(this);
    this.handleCloseFormClick = this.handleCloseFormClick.bind(this);
    this.state = { ShowForm: false };
    this.state = {
      currentView: "logIn"
    }
  }

  changeView = (view) => {
    this.setState({
      currentView: view
    })
  }

  handleLogin = () => {
    this.props.handleLogin();
    this.setState({ ShowForm: false });
  }

  handleRegister = () => {
    this.props.handleLogin();
    this.setState({ ShowForm: false });
  }

  handleLogout = () =>{
    window.alert('Do you want to Log out?');
    this.props.handleLogout();
    this.setState({ ShowForm: false });
  }

  handleShowForm() {
    this.setState({ShowForm: true});
  }

  handleCloseFormClick() {
    this.setState({ShowForm: false});
  }

  currentView = () => {

    if(this.state.currentView === 'logIn') {
        return (
          <div className="login-section">
            <form className="login-form">
              <h2 className="login-heading">LOGIN</h2>
              <div className="login-group">
                <label className="login-label">Email</label>
                <input type="email" className="login-input" />
              </div>
              <div className="login-group">
                <label className="login-label">Password</label>
                <input type="password" className="login-input" />
              </div>
              <LoginBtn onClick={this.handleLogin} />
              <div className="login-registration">
                No account yet? <button type="button" className="login-register-btn" onClick={ () => this.changeView("register")}>REGISTER HERE</button>
              </div>
            </form>
          </div>
        )
      } else {
          return (
            <div className="login-section">
              <form className="login-form">
                <h2 className="login-heading">REGISTER</h2>
                <div className="login-group">
                  <label className="login-label">Email</label>
                  <input type="email" className="login-input" />
                </div>
                <div className="login-group">
                  <label className="login-label">Password</label>
                  <input type="password" className="login-input" />
                </div>
                <div className="login-group">
                  <label className="login-label">Confirm Password</label>
                  <input type="password" className="login-input" />
                </div>
                <RegisterBtn onClick={this.handleRegister} />
                <div className="login-registration">
                  Already have an account? <button className="login-register-btn" type="button" onClick={ () => this.changeView("logIn")}>LOGIN HERE</button>
                </div>
              </form>
            </div>
          )
    }

  }


  render () {

    const LoggedIn = this.props.LoggedIn;
    const ShowForm = this.state.ShowForm;
    let button;

    if (LoggedIn) {
      button = <LogoutBtn onClick={this.handleLogout} />;
    } else if (ShowForm){
      button = <CloseFormDetail onClick={this.handleCloseFormClick} />;
    } else {
      button = <OpenFormDetail onClick={this.handleShowForm} />;
    }


    return (
      <>
        {button}
        { ShowForm ? this.currentView() : null }
      </>
    )
  }
}

function OpenFormDetail(props) {
  return (
    <button className="form-header" onClick={props.onClick}>
      LOGIN
    </button>
  );
}

function CloseFormDetail(props) {
  return (
    <button className="form-header" onClick={props.onClick}>
      CLOSE
    </button>
  );
}

function LogoutBtn(props) {
  return (
    <button className="form-header" onClick={props.onClick}>
      LOGOUT
    </button>
  );
}
function LoginBtn(props) {
  return (
    <button className="login-btn" onClick={props.onClick}>
      LOGIN
    </button>
  );
}

function RegisterBtn(props) {
  return (
    <button className="login-btn" onClick={props.onClick}>
      REGISTER
    </button>
  );
}

export default Login;
