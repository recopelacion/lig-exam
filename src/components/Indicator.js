import React, { Component } from 'react'

class Indicator extends Component {

    constructor(props){
        super(props);

        this.state = {
            activeIndex: 0
        }
    }

    goToSlide(index) {
        this.state = {
            activeIndex: index
        }
    }

    render() {
        return (
            <li key={index}>
                <a className={this.props.index == this.props.activeIndex ? "indicator indicator-active" : "indicator"} onClick={this.props.onClick}></a>
            </li>
        )
    }
}

export default Indicator
