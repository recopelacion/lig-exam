import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

import Posts from '../components/Posts';
import posts from "./postsArray";

const postsPerPage = 6;
let arrayForHoldingPosts = [];

const News = (props) => {
  const [postsToShow, setPostsToShow] = useState([]);
  const [next, setNext] = useState(3);

  const loopWithSlice = (start, end) => {
    const slicedPosts = posts.slice(start, end);
    arrayForHoldingPosts = [...arrayForHoldingPosts, ...slicedPosts];
    setPostsToShow(arrayForHoldingPosts);
  };

  useEffect(() => {
    loopWithSlice(0, postsPerPage);
  }, []);

  const handleShowMorePosts = () => {
    loopWithSlice(next, next + postsPerPage);
    setNext(next + postsPerPage);
  };

  const scrollTop = () => {
     window.scrollTo({top: 0, behavior: 'smooth'});
  };


  const LoggedIn = props.LoggedIn;

    return (

      <div className="news-wrapper">
        <div className="cntr">
            <div className="newsHeader">
              <h3 className="main-title">NEWS</h3>
              { LoggedIn ?
                <Link className="news-head-link" to="/create-post" onClick={scrollTop}>Create New Post</Link>
              : null }
            </div>
            <Posts postsToRender={postsToShow} />
            <div className="btn">
                <button className="load-more" onClick={handleShowMorePosts}>
                    LOAD MORE
                </button>
            </div>
        </div>
      </div>

    )
}

export default News;
