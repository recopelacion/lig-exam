import React from 'react';
import { Link } from "react-router-dom";
import posts from "./postsArray";

const Articles = (props) => {
  const id = props.id;
  const {img, date, title} = props.item;

  const scrollTop = () => {
     window.scrollTo({top: 0, behavior: 'smooth'});
  };

  return(
    <div key={`posts-${id}`} className="md-4 xs-12">
        <Link to={`/news/${title}`} className="card" onClick={scrollTop}>
            <div className="card-img">
                <img src={img} alt={img} />
            </div>
            <div className="card-content">
                <span className="card-date">{date}</span>
                <h3 className="card-title">{title}</h3>
            </div>
        </Link>
    </div>
  )
}
export default Articles;
