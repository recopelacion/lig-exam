import React, { useState } from 'react'

function Hero() {

    let sliderArr = [
        "/assets/images/hero-img.png",
        "/assets/images/hero-img.png",
        "/assets/images/hero-img.png"
    ];
    const [x, setX] = useState(0);
    const prevButton = () => {
        x === 0 ? setX(-100 * (sliderArr.length - 1)) : setX(x + 100);
    };
    const nextButton = () => {
        x === -100 * (sliderArr.length - 1) ? setX(0) : setX(x - 100);
    };
    
    const goSlide = () => {
        
    }

    return (
        <div className="slider-wrapper">
            {
                sliderArr.map((item, index) =>{
                    return(
                        <div key={index} className="slider-item" style={{transform: `translateX(${x}%)`}}>
                            <div className="slider-img">
                                <img src={item} alt="slider image" />
                            </div>
                            <div className="slider-content">
                                <h4>
                                    <span>サンプルテキスト</span><br />
                                    <span>サンプル ルテキストサン</span><br />
                                    <span>サンプルテキスト</span>
                                </h4>
                                <time className="slide-date">2019.06.19</time>
                            </div>
                        </div>
                    )
                })
            }
            <button className="prevButton" onClick={prevButton}><img src={process.env.PUBLIC_URL + '/assets/images/prev.png'} alt="Prev Icon" /></button>
            <button className="nextButton" onClick={nextButton}><img src={process.env.PUBLIC_URL + '/assets/images/next.png'} alt="Next Icon" /></button>
            <ul class="carousel-indicator">
                {
                    sliderArr.map((item, index) => {
                        return(
                            <li key={index}>
                                <a className={index === x ? "indicator indicator-active" : "indicator"} onClick={e => goSlide(index)}></a>
                            </li>
                        )
                    })
                }
            </ul>
        </div>
    )
}

export default Hero
