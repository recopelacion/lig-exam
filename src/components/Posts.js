import React from "react";
import { Link } from 'react-router-dom';

import Articles from '../components/Articles';

const Posts = ({ postsToRender }) => {

  return (


  <div className="gap gap-25 gap-0-xs">
    {postsToRender.map((post, id) => (
      <Articles item={post} id={id} />
    ))}
  </div>


  );
};

export default Posts;
