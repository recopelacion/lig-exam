import React from 'react';
import { Link } from 'react-router-dom';

import Login from '../components/Login';

function Header(props) {
    return (
      <header className="main-header">
        <div className="cntr">
            <div className="main-header-group">
                <Link to="/" className="main-logo">
                    <img src={process.env.PUBLIC_URL + '/assets/images/logo.png'} alt="Logo" />
                </Link>
                <div className="main-nav">
                  <Login LoggedIn={props.LoggedIn} handleLogin={props.handleLogin} handleLogout={props.handleLogout}/>
                </div>
            </div>
        </div>
    </header>
    )
}

export default Header;
