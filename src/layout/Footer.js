import React from 'react';
import { Link } from 'react-router-dom';

function Footer() {

  const scrollTop = () => {
     window.scrollTo({top: 0, behavior: 'smooth'});
  };

  return (
      <footer className="main-footer">
          <div className="cntr">
              <div className="main-logo-footer">
                  <Link to="/" className="footer-logo">
                      <img src={process.env.PUBLIC_URL + '/assets/images/logo.png'} alt="Logo" />
                  </Link>
                  <h4>
                      サンプルテキストサンプル ルテキストサンプルテキストサンプルテキス<br />
                      トサンプル ルテキスト
                  </h4>
                  <div className="scrollTop">
                      <button onClick={scrollTop} className="scrollBtn">
                          <i className="scrollArrow"><img src={process.env.PUBLIC_URL + '/assets/images/icon.png'} alt="Scroll top" /></i>
                          Top
                      </button>
                  </div>
              </div>
          </div>
          <div className="copyright">
              <div className="cntr">
                  <p>Copyright©2007-2019 Blog Inc.</p>
              </div>
          </div>
      </footer>
  )
}

 export default Footer;
