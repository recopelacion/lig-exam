import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Top from '../pages/Top';
import CreateNewPost from '../pages/CreateNewPost';
import NewsDetail from '../pages/NewsDetail';

function Main(props) {
  return (
    <Switch>
      <Route path="/create-post">
        <CreateNewPost />
      </Route>
      <Route path="/news/:detail">
        <NewsDetail LoggedIn={props.LoggedIn} />
      </Route>
      <Route path="/">
        <Top LoggedIn={props.LoggedIn} />
      </Route>
    </Switch>
  )
}

export default Main;
